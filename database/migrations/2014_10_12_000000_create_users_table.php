<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('id', 45)->primary();
            $table->string('first_name', 191)->nullable();
            $table->string('last_name', 191)->nullable();
            $table->string('gender', 191)->nullable();
            $table->string('mobile_country_code', 191)->nullable();
            $table->string('mobile', 191)->nullable();
            $table->timestamp('mobile_verified_at')->nullable();
            $table->string('id_type', 191)->nullable();
            $table->string('id_number', 191)->nullable();
            $table->boolean('sign_up_complete')->default(0);
            $table->string('id_exp_date', 191)->nullable();
            $table->string('id_issuing_country', 191)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('nationality', 191)->nullable();
            $table->string('country_of_residence', 191)->nullable();
            $table->string('address_number', 191)->nullable();
            $table->string('address_refinement', 191)->nullable();
            $table->string('address_block_number', 200)->nullable();
            $table->string('address_region', 191)->nullable();
            $table->string('address_street', 191)->nullable();
            $table->string('address_city', 191)->nullable();
            $table->string('address_postal_code', 191)->nullable();
            $table->string('address_country_code', 191)->nullable();
            $table->string('password', 191)->nullable();
            $table->string('kyc_status', 191)->default('NOT_VERIFIED');
            $table->string('kyc_status_source', 191)->nullable();
            $table->dateTime('kyc_updated_at')->nullable();
            $table->string('email', 191)->nullable();
            $table->dateTime('email_verified_at')->nullable();
            $table->boolean('marketing_consent_email')->default(0);
            $table->boolean('marketing_consent_mobile')->default(0);
            $table->string('avatar', 191)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
