<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_experiences', function (Blueprint $table) {
            $table->string('id', 45)->primary();
            $table->string('user_id', 191);
            $table->boolean('traded_six_times_last_three_years')->default(0);
            $table->boolean('higher_qualification_in_finance_related')->nullable();
            $table->boolean('any_finance_related_qualification')->nullable();
            $table->boolean('relevant_working_experience_three_years')->nullable();
            $table->boolean('online_education_sgx')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_experiences');
    }
}
