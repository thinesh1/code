<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_statuses', function (Blueprint $table) {
            $table->string('id', 45)->primary();
            $table->string('user_id', 191);
            $table->string('employment_status', 191);
            $table->boolean('related_to_publicly_traded_company')->default(0);
            $table->boolean('work_for_another_stock_broker')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_statuses');
    }
}
