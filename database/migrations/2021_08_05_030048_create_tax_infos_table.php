<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_infos', function (Blueprint $table) {
            $table->string('id', 45)->primary();
            $table->string('user_id', 191);
            $table->boolean('us_or_permanent_resident')->default(0);
            $table->boolean('tax_different_from_country')->default(1);
            $table->boolean('multiple_tax_residencies')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_infos');
    }
}
