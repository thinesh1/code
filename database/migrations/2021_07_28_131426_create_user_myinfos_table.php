<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMyinfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_myinfos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id', 45)->references('id')->on('users');
            $table->string('myinfo_ref', 191);
            $table->text('myinfo_response');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_myinfos');
    }
}
