<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_statuses', function (Blueprint $table) {
            $table->string('id', 45)->primary();
            $table->string('user_id', 191);
            $table->boolean('last_year_earning')->default(0);
            $table->boolean('financial_assets')->default(0);
            $table->boolean('personal_assets')->default(0);
            $table->boolean('checkbox')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_statuses');
    }
}
