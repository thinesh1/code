<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmpStatus extends BaseModel
{
    use HasFactory;

    protected $keyType = 'string';

    protected $fillable =[
        'user_id','employment_status','related_to_publicly_traded_company','work_for_another_stock_broker'
    ];
}
