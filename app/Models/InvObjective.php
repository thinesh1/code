<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvObjective extends BaseModel
{
    use HasFactory;
    protected $keyType = 'string';

    protected $fillable =[
        'user_id','investment_objectives','investor_type',
    ];
}
