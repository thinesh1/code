<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SourceOfWealth extends BaseModel
{
    use HasFactory;

    protected $keyType = 'string';

    protected $fillable =[
        'emp_details_id','source_of_wealth'
    ];
}
