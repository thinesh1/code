<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvStatus extends BaseModel
{
    use HasFactory;

    protected $keyType = 'string';

    protected $fillable =[
        'last_year_earning','financial_assets','personal_assets','checkbox','user_id',
    ];
}
