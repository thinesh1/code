<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class InvExperience extends BaseModel
{
    use HasFactory;
    protected $keyType = 'string';

    protected $fillable =[
        'traded_six_times_last_three_years','higher_qualification_in_finance_related','any_finance_related_qualification',
        'relevant_working_experience_three_years','online_education_sgx','user_id',
    ];
}
