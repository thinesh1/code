<?php

namespace App\Models;

class Token extends BaseModel
{

    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token',
        'user_id'
    ];

}
