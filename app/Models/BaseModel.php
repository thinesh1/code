<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class BaseModel extends Model
{

    public $incrementing = false;

    /**
     * Boot the model and bind event handlers.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $keyName = $model->getKeyName();

            $model->$keyName = $model->newUuid();
        });

    }

    /**
     * Generates a new UUID.
     *
     * @throws \Exception
     *
     * @return string
     */
    protected function newUuid()
    {
        return (string) Uuid::generate(4);
    }
}
