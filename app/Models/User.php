<?php

namespace App\Models;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    JWTSubject
{
    use Notifiable, \Illuminate\Auth\Authenticatable, Authorizable, CanResetPassword;

    const NOT_VERIFIED = 'NOT_VERIFIED';
    const VERIFIED = 'VERIFIED';
    const PENDING = 'PENDING';
    const REJECTED = 'REJECTED';

    const KYC_SOURCE_ONFIDO = 'KYC_SOURCE_ONFIDO';

    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'gender',
        'mobile_country_code',
        'mobile',
        'mobile_verified_at',
        'id_type',
        'id_number',
        'date_of_birth',
        'nationality',
        'address_number',
        'address_refinement',
        'address_block_number',
        'address_region',
        'address_street',
        'address_city',
        'address_postal_code',
        'address_country_code',
        'password',
        'kyc_status',
        'kyc_status_source',
        'kyc_updated_at',
        'email',
        'email_verified_at',
        'marketing_consent_email',
        'marketing_consent_mobile',
        'sign_up_complete',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'mobile_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
