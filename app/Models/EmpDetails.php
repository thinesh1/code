<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmpDetails extends BaseModel
{
    use HasFactory;
    protected $keyType = 'string';

    protected $fillable =[
        'user_id','industry','employer_name','occupation','annual_income','net_worth',
    ];

  //  protected $casts = [
   //     'source_of_wealth' => 'array'
   // ];

}
