<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxInfo extends BaseModel
{
    use HasFactory;

    protected $keyType = 'string';

    protected $fillable =[
        'us_or_permanent_resident','tax_different_from_country','multiple_tax_residencies','user_id'
    ];
}
