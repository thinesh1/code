<?php

namespace App\Helpers;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;

class TokenHelper
{
    public static function create($data, $expires = null)
    {
        if (is_null($expires)) {
            $expires = Carbon::now()->addMinute()->timestamp;
        }

        return encrypt((object)[
            'data' => $data,
            'expires' => $expires,
        ]);
    }

    public static function decrypt($token)
    {
        return decrypt($token);
    }

    public static function verify($token, $data)
    {
        try {
            $decrypted = self::decrypt($token);
            return ($decrypted->expires > Carbon::now()->timestamp || $decrypted->expires === 0) && $decrypted->data == $data;

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }
}
