<?php

namespace App\Helpers;

use Exception;

class CountriesHelper
{
	public static function ISO3ToIDD($IDDCode)
	{
		return self::getCountryByISO3($IDDCode)->iddCode;
	}

    public static function IDDCodeToISO3($IDDCode)
    {
        return self::getCountryByIDDCode($IDDCode)->ISO3;
    }

    public static function IDDCodeToISO2($IDDCode)
    {
        return self::getCountryByIDDCode($IDDCode)->ISO2;
    }

    public static function ISO3ToISO2($ISO3){

        return self::getCountryByISO3($ISO3)->ISO2;
    }

    public static function ISO2ToISO3($ISO2){

        return self::getCountryByISO2($ISO2)->ISO3;
    }

    public static function getCountryByISO3($ISO3)
    {
        return self::get('ISO3', strtoupper($ISO3));
    }

    public static function getCountryByISO2($ISO2)
    {
        return self::get('ISO2', strtoupper($ISO2));
    }

    public static function getCountries()
    {
        return collect(config('countries'))->map(function($country){

            return (object)$country;
        });
    }

    public static function getCountryByIDDCode($IDDCode)
    {
        return self::get('iddCode', strtoupper($IDDCode));
    }

    private static function get($key, $value)
    {
        $country = self::getCountries()->where($key, $value)->first();

        if(!$country){

            throw new Exception('country with ' . $key . ' : ' . $value . ' not found');
        }

        return $country;
    }
}
