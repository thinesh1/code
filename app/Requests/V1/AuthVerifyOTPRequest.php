<?php

namespace App\Requests\V1;

use App\Requests\V1\BaseRequest as FormRequest;

class AuthVerifyOTPRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uuid' => 'required',
            'userId' => 'required',
            'otp' => 'required',
        ];
    }
}
