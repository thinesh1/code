<?php

namespace App\Requests\V1;

use App\Requests\V1\BaseRequest as FormRequest;

class EmpDetailsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            'industry' => 'required',
            'employerName' => 'required',
            'occupation' => 'required',
            'annualIncome' => 'required',
            'netWorth' => 'required',
            'sourceOfWealth' => 'required',
        ];
    }
}
