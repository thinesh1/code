<?php

namespace App\Requests\V1;

use App\Requests\V1\BaseRequest as FormRequest;

class AccountUpdateUserRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required',
            'lastName' => 'required',
            'gender' => 'required',
            //'dateOfBirth' => 'required|date|olderThan:0',
            'dateOfBirth' => '',
            'nationality' => 'required',
            'addressNumber' => '',
            'addressRefinement' => '',
            'addressBlockNumber' => '',
            'addressRegion' => '',
            'idNumber' => '',
            'idType' => '',
            'addressStreet' => 'required',
            'addressCity' => '',
            'addressPostalCode' => 'required',
            'addressCountryCode' => 'required|countryCode',
        ];
    }
}
