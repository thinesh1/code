<?php

namespace App\Requests\V1;

use App\Requests\V1\BaseRequest as FormRequest;

class InvExperienceRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            'tradedSixTimesLastThreeYears' => 'required',
            'higherQualificationInFinanceRelated' => '',
            'anyFinanceRelatedQualification' => '',
            'relevantWorkingExperienceOfThreeYears' => '',
            'onlineEducationSGX' => '',
        ];
    }
}
