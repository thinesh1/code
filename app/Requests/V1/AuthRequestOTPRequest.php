<?php

namespace App\Requests\V1;

use App\Requests\V1\BaseRequest as FormRequest;

class AuthRequestOTPRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userId' => 'required',
//            'mobile' => 'required',
//            'mobileCountryCode' => 'required',
        ];
    }
}
