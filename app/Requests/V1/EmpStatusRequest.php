<?php

namespace App\Requests\V1;

use App\Requests\V1\BaseRequest as FormRequest;

class EmpStatusRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            'employmentStatus' => 'required',
            'relatedToPubliclyTradedCompany' => 'required',
            'workForAnotherStockBroker' => 'required',
        ];
    }
}
