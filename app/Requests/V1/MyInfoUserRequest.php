<?php

namespace App\Requests\V1;

use App\Requests\V1\BaseRequest as FormRequest;

class MyInfoUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required'
        ];
    }
}
