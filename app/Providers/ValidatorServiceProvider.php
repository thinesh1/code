<?php

namespace App\Providers;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Helpers\CountriesHelper;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Unique mobile country code + mobile and validated validation
        $this->app['validator']->extend('uniqueVerifiedMobile', function ($attribute, $value, $parameters, Validator $validator) {

            $data = $validator->getData();

            $mobileCountryCode = $data['mobileCountryCode'] ?? null;
            $mobile = $data['mobile'] ?? null;

            $count = DB::table('users')
                ->where('mobile_country_code', $mobileCountryCode)
                ->where('mobile', $mobile)
                ->whereNotNull('mobile_verified_at')
                ->count();

            return $count == 0;
        });

        $this->app['validator']->extend('uniqueMobile', function ($attribute, $value, $parameters, Validator $validator) {

            $data = $validator->getData();

            $mobileCountryCode = $data['mobileCountryCode'] ?? null;
            $mobile = $data['mobile'] ?? null;

            $count = DB::table('users')
                ->where('mobile_country_code', $mobileCountryCode)
                ->where('mobile', $mobile)
                ->count();

            return $count == 0;
        });

        $this->app['validator']->extend('countryCode', function ($attribute, $value, $parameters)
        {
            try{
                return (bool)CountriesHelper::getCountryByISO2($value);

            }catch(Exception $e){

                return false;
            }
        });

    }
}
