<?php

namespace App\Providers;


use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\TokenRepositoryInterface;
use App\Repositories\TokenRepository;
use App\Repositories\UserRepository;
use App\Services\Interfaces\OTPServiceInterface;
use App\Services\NexmoOTPService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Repositories\Interfaces\TaxInfoRepositoryInterface;
use App\Repositories\TaxInfoRepository;
use App\Repositories\Interfaces\InvStatusRepositoryInterface;
use App\Repositories\InvStatusRepository;
use App\Repositories\Interfaces\EmpStatusRepositoryInterface;
use App\Repositories\EmpStatusRepository;
use App\Repositories\Interfaces\InvExperienceRepositoryInterface;
use App\Repositories\InvExperienceRepository;
use App\Repositories\Interfaces\InvObjectiveRepositoryInterface;
use App\Repositories\InvObjectiveRepository;
use App\Repositories\Interfaces\EmpDetailsRepositoryInterface;
use App\Repositories\EmpDetailsRepository;


class AppServiceProvider extends ServiceProvider
{
    public $bindings = [
            UserRepositoryInterface::class => UserRepository::class,
            TokenRepositoryInterface::class => TokenRepository::class,
            OTPServiceInterface::class => NexmoOTPService::class,
            TaxInfoRepositoryInterface::class=>TaxInfoRepository::class,
            InvStatusRepositoryInterface::class=>InvStatusRepository::class,
            EmpStatusRepositoryInterface::class=>EmpStatusRepository::class,
            InvExperienceRepositoryInterface::class=>InvExperienceRepository::class,
            InvObjectiveRepositoryInterface::class=>InvObjectiveRepository::class,
            EmpDetailsRepositoryInterface::class=>EmpDetailsRepository::class
        ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
