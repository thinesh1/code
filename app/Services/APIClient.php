<?php

namespace App\Services;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class APIClient
{
    protected $client;
    protected $headers = [];

    private $jsonRequest = true;

    /**
     * @param bool $jsonRequest
     */
    public function setJsonRequest(bool $jsonRequest): void
    {
        $this->jsonRequest = $jsonRequest;
    }

    protected function getBaseUrl()
    {
    	throw new Exception(__CLASS__ . ' : Base url not set');
    }

    protected function addHeader($header, $value)
    {
        $this->headers[$header] = $value;
    }

    protected function getRequestHeaders()
    {
        if($this->jsonRequest){
            $this->addHeader('Content-Type', 'application/json');
        };

        return $this->headers;
    }

    public function put($uri, $formData = null)
    {
        return $this->request('PUT', $uri, $formData);
    }

    public function post($uri, $formData = null)
    {
        return $this->request('POST', $uri, $formData);
    }

    public function get($uri, $queryParams = null)
    {
        return $this->request('GET', $uri, $queryParams);
    }

    public function request($method, $uri, $formData = null)
    {
        $options = [
            'headers' => $this->getRequestHeaders()
        ];

        if($formData && $method !== 'GET'){
            if($this->jsonRequest){
                $options['body'] = json_encode($formData);
            }else{
                $options['form_params'] = $formData;
            }
        }

        if($formData && $method === 'GET'){
            $options['query'] = $formData;
        }

        Log::debug('HTTP request: ' . $method . ' ' . $uri , $this->maskValues($options));

        $response = $this->client->request($method, $uri, $options);

		$response = $this->getDecodedResponse($response);

		Log::debug('HTTP response: ' . $method . ' ' . $uri , $this->maskValues((Array)$response));

		return $response;
    }

    private function maskValues($data){

        if(isset($data['body'])){
            $data['body'] = json_decode($data['body']);
        }

        $string = json_encode($data);

        $hiddenFields = [
            'documentNumber',
            'emailAddress'
        ];

        $pattern = '/\\\\?"(' . implode("|", $hiddenFields) . ')\\\\?":\\\\?"([a-zA-Z0-9\.@]+)\\\\?"/m';

        return json_decode(preg_replace($pattern, "\"$1\":\"xxxxxx\"", $string), true);
    }

    public function getDecodedResponse($response)
    {
        return json_decode($response->getBody()->getContents());
    }

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => $this->getBaseUrl()
        ]);
    }
}
