<?php

namespace App\Services;

use App\Services\Interfaces\OTPServiceInterface;
#use App\Services\NexmoService;
use Illuminate\Support\Facades\Log;

class NexmoOTPService implements OTPServiceInterface
{
    protected $nexmoService;

    public function verifyOTP($uuid, $otp)
    {
        $response = $this->nexmoService->verifyOTP($uuid, $otp);

        Log::debug(json_encode($response));

        return $response->status === '0';
    }

    public function sendOTP($mobileNumber)
    {
        $response = $this->nexmoService->requestOTP($mobileNumber);

        if ($response->status != 0) {
            throw new \Exception($response->error_text);
        }

        return $response->request_id;
    }

    public function __construct()
    {
        $this->nexmoService = new NexmoService();
    }
}
