<?php

namespace App\Services;

#use App\Extensions\ProLog;
use Dingo\Api\Facade\API;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use App\Services\APIClient;
use Onfido\Api\DefaultApi;
use Onfido\Configuration;
use Onfido\Model\Applicant;
use Onfido\Model\SdkToken;
use Onfido\Model\Check;
use Exception;

class OnfidoService
{
	protected $apiInstance;

	public function __construct()
    {
		$config = Configuration::getDefaultConfiguration();

		$config->setApiKey('Authorization', 'token=' . $this->getOnfidoApiKey());

		$config->setApiKeyPrefix('Authorization', 'Token');

		$this->apiInstance = new DefaultApi(null, $config);

	}

	public function createApplicant($firstName, $lastName)
	{
		if (empty($firstName) || empty($lastName)) {
			Log::error('Missing user details');
            return;
		}

		$applicantId = "";

		try {
			$applicant = new Applicant();
			$applicant->setFirstName($firstName);
			$applicant->setLastName($lastName);

			$applicantResponse = $this->apiInstance->createApplicant($applicant);
			$applicantId = $applicantResponse->getId();

			Log::info("Onfido Applicant ID: ".$applicantId);
		} catch (Exception $e) {
			Log::info("Exception when calling DefaultApi->createApplicant: ".$e->getMessage());
		}

		return $applicantId;
	}

	public function createCheck($applicantId)
	{
		if (empty($applicantId)) {
			Log::error('Missing applicant id');
			return false;
		}

		$checkStatus = false;

		try {

			$checkData = new Check();
			//$checkData->setReportNames(array('identity_standard'));

            $checkData->setReportNames(['document']);
			$checkData->setApplicantId($applicantId);
			$checkResult = $this->apiInstance->createCheck($checkData);

			//$checkId = $checkResult->getId();
			//$reportIds = $checkResult->getReportIds();

			$checkStatus = $checkResult->getStatus();
            Log::info($checkStatus);

		} catch (Exception $e) {
            Log::info($e->getMessage());
		}

		return $checkStatus;
	}

	public function findCheck($checkId, $returnCheckObject = false)
	{
		if (empty($checkId)) {
			Log::error('Missing check id');
			return '';
		}

		$check = null;

		try {
			$check = $this->apiInstance->findCheck($checkId);
			$checkId = $check->getId();

			Log::info("Onfido Check ID: ".$checkId);
		} catch (Exception $e) {
			Log::info("Exception when calling DefaultApi->createCheck: ".$e->getMessage());
			return '';
		}

		if ($returnCheckObject) {
		    return $check;
        }
		return $checkId;
	}

	public function getOnfidoSdkToken($applicantId)
	{
		if (empty($applicantId)) {
			Log::error('Missing applicant id');
			return;
		}

		$sdkToken = "";

		try {
			$sdkTokenObject = new SdkToken();
			$sdkTokenObject->setApplicantId($applicantId);
			$sdkTokenObject->setApplicationId($this->getApplicationId());

			$sdkTokenResponse = $this->apiInstance->generateSdkToken($sdkTokenObject);
			$sdkToken = $sdkTokenResponse->getToken();

			Log::info("Onfido SDK Token: ".$sdkToken);
		} catch (Exception $e) {
			Log::info("Exception when calling DefaultApi->generateSdkToken: ".$e->getMessage());
		}

		return $sdkToken;
	}

	private function getApplicationId()
    {
		return config('services.onfido.application_id');
	}

	private function getOnfidoApiKey()
    {
		return config('services.onfido.api_key');
    }
}
