<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use App\Services\APIClient;

class NexmoService extends APIClient
{
    public function verifyOTP($requestId, $otp)
    {
        return $this->post('verify/check/json', array_merge($this->getCommonRequestParams(), [
            'request_id' => $requestId,
            'code' => $otp
        ]));
    }

    public function requestOTP($mobileNumber)
    {
        return $this->post('verify/json', array_merge($this->getCommonRequestParams(), [
            'number' => $mobileNumber,
            'brand' => config('app.name'),
            'code_length' => config('services.nexmo.code_length'),
            'sender_id' => config('services.nexmo.sender_id'),
            'pin_expiry' => config('services.nexmo.pin_expiry'),
            'next_event_wait' => config('services.nexmo.next_event_wait'),
            'workflow_id' => config('services.nexmo.workflow_id'),

        ]));
    }

    public function sendSMS($mobileNumber, $message)
    {
        return $this->post('sms/json', array_merge($this->getCommonRequestParams(), [
            'to' => $mobileNumber,
            'from' => config('app.name'),
            'text' => $message,
        ]));
    }

    protected function getCommonRequestParams()
    {
        return [
            'api_key' => $this->getAPIKey(),
            'api_secret' => $this->getAPISecret(),
        ];
    }

    protected function getBaseUrl()
    {
        return config('services.nexmo.api_base_url');
    }

    protected function getAPIKey()
    {
        return config('services.nexmo.api_key');
    }

    protected function getAPISecret()
    {
        return config('services.nexmo.api_secret');
    }

    protected function getFrom()
    {
        return config('services.nexmo.from');
    }
}
