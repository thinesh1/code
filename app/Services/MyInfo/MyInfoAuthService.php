<?php

namespace App\Services\MyInfo;

use Carbon\Carbon;
//use Firebase\JWT\JWT;
use Tymon\JWTAuth\JWT;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Query;
use Illuminate\Support\Facades\Log;
use App\Services\APIClient;

class MyInfoAuthService extends APIClient
{
    public $token;

    public function setAccessToken($code)
    {
        Log::info('getAccessToken ============ ');

        $this->token = $this->getAccessToken($code);

        Log::info('getAccessToken Done ============ ');
        $this->addHeader('Authorization', 'Bearer ' . $this->token->access_token);
    }

    private function getAccessToken($code)
    {
        $this->setJsonRequest(false);

        $this->addHeader('Cache-Control', 'no-cache');
        $this->addHeader('Content-Type', 'application/x-www-form-urlencoded');
        $this->addHeader('Accept-Encoding', 'gzip');

        $response = $this->post($this->getBaseUrl() . 'token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'redirect_uri' => $this->getRedirectUrl(),
                'code' => $code,
                'client_id' => $this->getClientId(),
                'client_secret' => $this->getClientSecret(),
            ]
        ]);

        $this->token = $this->getDecodedResponse($response);

        return $this->token;
    }

    public function getAuthenticationUrl()
    {
        return $this->getBaseUrl() . 'authorise?' . http_build_query([
                'attributes' => implode(',', $this->getScope()),
                'response_type' => 'code',
                'client_id' => $this->getClientId(),
                'redirect_uri' => $this->getRedirectUrl(),
                'purpose' => $this->getPurpose(),
                'state' => 'state',
            ]);
    }

    private function getClientId()
    {
        return config('services.myinfo.client_id');
    }

    private function getClientSecret()
    {
        return config('services.myinfo.secret');
    }

    private function getClientAuthLevel()
    {
        return config('services.myinfo.client_auth_level');
    }

    private function getScope()
    {
        return [
            'uinfin',
            'name',
            'sex',
            'dob',
            'nationality',
//            'birthcountry',
            //'passexpirydate',
            'regadd',
        ];
    }

    private function getRedirectUrl()
    {
        return config('services.myinfo.callback');
    }

    protected function getBaseUrl()
    {
        return config('services.myinfo.base_url');
    }

    protected function getPurpose()
    {
        return config('services.myinfo.purpose');
    }

    public function getLogoutUrl()
    {
    }

    private function getDecodedToken()
    {
        $publicCert = file_get_contents(storage_path(config('services.myinfo.publicCertPath')));
//        return JWT::decode($this->token->access_token, config('services.myinfo.publicCert'), ['RS256']);
        return JWT::decode($this->token->access_token, $publicCert, ['RS256']);
    }

    public function getUserUINFIN()
    {
        return $this->getDecodedToken()->sub;
    }

    private function getRequestQueryParams()
    {
        return Query::build([
            'client_id' => $this->getClientId(),
            'attributes' => implode(',', $this->getScope()),
        ]);
    }

    public function getUserInfo()
    {
        $response = $this->get('person/' . $this->getUserUINFIN() . '?' . $this->getRequestQueryParams());
        $response->uinfin = $this->getUserUINFIN();
        return $response;
    }

    public function getUserInfoWithCode($code)
    {
        $this->setAccessToken($code);
        $response = $this->get('person/' . $this->getUserUINFIN() . '?' . $this->getRequestQueryParams());
        $response->uinfin = $this->getUserUINFIN();
        return $response;
    }

    // copy pasta from ziming/laravel-myinfo-sg
    //TODO handle exceptions


    public function getMyinfoPersonData(string $code)
    {
        Log::debug("getMyinfoPersonData:" . $code);
        $tokenRequestResponse = $this->createTokenRequest($code);
        $tokenRequestResponseBody = $tokenRequestResponse->getBody();
        if ($tokenRequestResponseBody) {
            $decoded = json_decode($tokenRequestResponseBody, true);
            if ($decoded) {
                return $this->callPersonAPI($decoded['access_token']);
            }
        }
    }


    /**
     * Create Token Request.
     *
     * @param string $code
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    private function createTokenRequest(string $code)
    {

        Log::debug("createTokenRequest:" . $code);
        $guzzleClient = app(Client::class);
        $tokenUrl = $this->getBaseUrl() . 'token';
        $contentType = 'application/x-www-form-urlencoded';
        $method = 'POST';

        $params = [
            'grant_type' => 'authorization_code',
            'redirect_uri' => $this->getRedirectUrl(),
            'client_id' => $this->getClientId(),
            'client_secret' => $this->getClientSecret(),
            'code' => $code,
        ];

        $headers = [
            'Cache-Control' => 'no-cache',
            'Content-Type' => $contentType,
            'Accept-Encoding' => 'gzip',
        ];

        Log::debug('-- Token Call --');
        Log::debug('Server Call Time: ' . Carbon::now()->toDayDateTimeString());
        Log::debug('Authorisation Code: ' . $code);
        Log::debug('Web Request URL: ' . $tokenUrl);


        if ($this->getClientAuthLevel() === 'L2') {

            Log::debug('-- Token Call - Security level L2 --');

            $authHeaders = MyInfoSecurityService::generateAuthorizationHeader(
                $tokenUrl,
                $params,
                $method,
                $contentType,
                $this->getClientAuthLevel(),
                $this->getClientId(),
                $this->getClientSecret()
            );

            $headers['Authorization'] = $authHeaders;
            Log::debug('Authorization Header: ' . $authHeaders);
        }

        $response = $guzzleClient->post($tokenUrl, [
            'form_params' => $params,
            'headers' => $headers,
        ]);

        return $response;
    }

    private function callPersonAPI($accessToken)
    {
        Log::debug('callPersonAPI: ' . $accessToken);
        $decoded = MyInfoSecurityService::verifyJWS($accessToken);

        if ($decoded === null) {
            throw new InvalidAccessTokenException;
        }

        $sub = $decoded['sub'];
        if ($sub === null) {
            throw new SubNotFoundException;
        }

        $personRequestResponse = $this->createPersonRequest($sub, $accessToken);
        $personRequestResponseBody = $personRequestResponse->getBody();
        $personRequestResponseContent = $personRequestResponseBody->getContents();

        if ($personRequestResponseContent) {
            $authLevel = $this->getClientAuthLevel();
            Log::debug("authLevel:" . $authLevel);

            if ($authLevel === 'L0') {
                return json_decode($personRequestResponseContent);

            } elseif ($authLevel === 'L2') {
                $personData = $personRequestResponseContent;
                $privateKeyPath = storage_path(config('services.myinfo.private_key_path'));

                $personDataJWS = MyInfoSecurityService::decryptJWE(
                    $personData,
                    $privateKeyPath
                );

                if ($personDataJWS === null) {
                    throw new InvalidDataOrSignatureForPersonDataException;
                }

                $decodedPersonData = MyInfoSecurityService::verifyJWS($personDataJWS, true);

                if ($decodedPersonData === null) {
                    throw new InvalidDataOrSignatureForPersonDataException;
                }

                return $decodedPersonData;
            }
        }
        throw new MyinfoPersonDataNotFoundException;
    }


    private function createPersonRequest($sub, $validAccessToken)
    {
        Log::debug('-- Person --' . $sub);
        $guzzleClient = app(Client::class);
        $url = $this->getBaseUrl() . "person/{$sub}/";

        $params = [
            'client_id' => $this->getClientId(),
            'attributes' => implode(',', $this->getScope()),
        ];

        $headers = [
            'Cache-Control' => 'no-cache',
            'Accept-Encoding' => 'gzip',
        ];

        Log::debug('-- Person Call --');
        Log::debug('Server Call Time: ' . Carbon::now()->toDayDateTimeString());
        Log::debug('Bearer Token: ' . $validAccessToken);
        Log::debug('Web Request URL: ' . $url);


        $authHeaders = MyInfoSecurityService::generateAuthorizationHeader(
            $url,
            $params,
            'GET',
            '',
            $this->getClientAuthLevel(),
            $this->getClientId(),
            $this->getClientSecret()
        );

        if ($authHeaders) {
            $headers['Authorization'] = $authHeaders . ',Bearer ' . $validAccessToken;
        } else {
            $headers['Authorization'] = 'Bearer ' . $validAccessToken;
        }

        Log::debug('-- Person Call --');
        Log::debug('Server Call Time: ' . Carbon::now()->toDayDateTimeString());
        Log::debug('Bearer Token: ' . $validAccessToken);
        Log::debug('Authorization Header: ' . $headers['Authorization']);

        $response = $guzzleClient->get($url, [
            'query' => $params,
            'headers' => $headers,
        ]);

        return $response;
    }

}
