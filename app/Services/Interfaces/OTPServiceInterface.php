<?php

namespace App\Services\Interfaces;

Interface OTPServiceInterface
{
    public function sendOTP($mobileNumber);

    public function verifyOTP($uuid, $otp);
}
