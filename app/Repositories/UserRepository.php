<?php

namespace App\Repositories;

use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class UserRepository implements UserRepositoryInterface
{
    public function createUnverifiedUser(
        $mobileCountryCode, $mobile, $password, $email, $marketingConsentEmail = 0,
        $marketingConsentMobile = 0)
    {
        $dataSet = [
            'mobile' => $mobile,
            'mobile_country_code' => $mobileCountryCode,
            'email' => $email,
            'marketing_consent_email' => $marketingConsentEmail,
            'marketing_consent_mobile' => $marketingConsentMobile,
            'password' => Hash::make($password),
            'sign_up_complete' => 1,
        ];

        return User::Create($dataSet);
    }

    public function getUser($userId)
    {
        return User::find($userId);
    }

    public function updateUser($userId, $data)
    {
        $user = $this->findUser($userId);
        $user->update($this->getUserModelFieldsFromRequestData($data));
        return $user->fresh();
    }

    public function findUser($userId)
    {
        return User::findOrFail($userId);
    }

    protected function getUserModelFieldsFromRequestData($data)
    {
        $returnedData = [];
        $fields = collect(array_keys($data))->except(['password']);

        foreach ($fields as $field) {
            $returnedData[Str::snake($field)] = $data[$field];
        }

        if (isset($data['password'])) {
            $returnedData['password'] = Hash::make($data['password']);
        }

        if (!isset($data['addressCity']) || empty($data['addressCity'])) {
            $returnedData['address_city'] = 'Singapore';
        }

        if (isset($data['dateOfBirth']) && !empty($data['dateOfBirth'])) {
            $dobForSQL = Carbon::parse($data['dateOfBirth'])->format('Y-m-d');
            $returnedData['date_of_birth'] = $dobForSQL;
            Log::info('DOB ' . $dobForSQL);
        }

        return $returnedData;

    }

    public function getUserByMobile($mobileCountryCode, $mobile)
    {
        return User::where([
            'mobile' => $mobile,
            'mobile_country_code' => $mobileCountryCode,
        ])->first();
    }

    public function updatePassword($password, $userId)
    {
        $user = $this->findUser($userId);
        $user->password = Hash::make($password);
        $user->save();
    }
}
