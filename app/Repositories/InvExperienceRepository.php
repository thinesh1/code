<?php

namespace App\Repositories;

use App\Repositories\Interfaces\InvExperienceRepositoryInterface;
use App\Models\InvExperience;

class InvExperienceRepository implements InvExperienceRepositoryInterface
{
    public function createInvExperience( $user_id,
    $tradedSixTimesLastThreeYears,
    $higherQualificationInFinanceRelated,
    $anyFinanceRelatedQualification,
    $relevantWorkingExperienceOfThreeYears,
    $onlineEducationSGX)
    {
        $dataSet = [
            'user_id' => $user_id,
            'traded_six_times_last_three_years' => $tradedSixTimesLastThreeYears,
            'higher_qualification_in_finance_related' => $higherQualificationInFinanceRelated,
            'any_finance_related_qualification' => $anyFinanceRelatedQualification,
            'relevant_working_experience_three_years' => $relevantWorkingExperienceOfThreeYears,
            'online_education_sgx' => $onlineEducationSGX
        ];
        return InvExperience::Create($dataSet);
    }
}
