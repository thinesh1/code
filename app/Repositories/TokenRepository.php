<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\TokenRepositoryInterface;
use App\Services\Interfaces\OTPServiceInterface;
use App\Models\Token;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Helpers\TokenHelper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;
use Webpatser\Uuid\Uuid;

class TokenRepository implements TokenRepositoryInterface
{
    protected $otpService;

    public function __construct(OTPServiceInterface $otpService)
    {
        $this->otpService = $otpService;
    }

    public function clearRegistrationToken($mobile, $mobileCountryCode)
    {
        DB::table('verified_mobiles')->delete([
            'to_mobile' => $mobile,
            'to_mobile_country_code' => $mobileCountryCode,
        ]);
    }

    public function createNewRegistrationToken($mobile, $mobileCountryCode)
    {
        return TokenHelper::create((object)[
            'auth' => 'register',
            'mobile' => $mobile,
            'mobile_country_code' => $mobileCountryCode,
        ], Carbon::now()->addMinutes(60)->timestamp);
    }

    public function requestOTP($mobileCountryCode, $mobile)
    {
        if (config('app.env') != 'production' && config('app.env') != 'staging') {
            Log::info('generating manual UUID skipping the OTP service in the environment ' . config('app.env'));
            return Uuid::generate(4)->string;
        }

        return $this->otpService->sendOTP($mobileCountryCode . $mobile);
    }

    public function verifyOTP($uuid, $otp, $mobile, $mobileCountryCode)
    {
        $result= false;
        if(config('app.env') == 'production' || config('app.env') == 'staging') {
            $result = $this->otpService->verifyOTP($uuid, $otp);
        }else{
            $result = true;
        }

        if($result){
            DB::table('verified_mobiles')->insert([
                'id' => Uuid::generate(4),
                'otp_uuid' => $uuid,
                'mobile' => $mobile,
                'mobile_country_code' => $mobileCountryCode,
                'created_at' => Carbon::now()
            ]);
        }

        return $result;
    }

    public function jwtAttempt($mobileCountryCode, $mobile, $password)
    {
        $accessToken = JWTAuth::attempt([
            'mobile_country_code' => $mobileCountryCode,
            'mobile' => $mobile,
            'password' => $password
        ]);

        if (!$accessToken) {

            return false;
        }

        $user = User::where(['mobile_country_code' => $mobileCountryCode, 'mobile' => $mobile])->first();
        return $this->createJwtResponseObject($user, $accessToken);
    }

    public function jwtRefresh($refreshToken)
    {
        $token = Token::where(['token' => $refreshToken])->first();

        if($token && $token->created_at->diffInDays() < 7){

            return [
                'tokenType' => 'Bearer',
                'expiresIn' => $this->getJWTTTL(),
                'accessToken' => JWTAuth::fromUser(User::find($token->user_id))
            ];
        }
    }

    private function createJwtResponseObject($user, $accessToken)
    {
        return (object)[
            'tokenType' => 'Bearer',
            'expiresIn' => $this->getJWTTTL(),
            'refreshToken' => $this->createRefreshToken($user->id)->token,
            'accessToken' => $accessToken,
            'userId' => $user->id
        ];
    }

    private function getJWTTTL()
    {
        return config('jwt.ttl');
    }

    private function createRefreshToken($userId)
    {
        $token = Token::where(['user_id' => $userId]);

        if ($token) {
            $token->delete();
        }

        return Token::create([
            'user_id' => $userId,
            'token' => hash_hmac('sha256', $userId . Str::random(10), config('app.key'))
        ]);
    }

    public function getPasswordResetToken($mobile, $mobileCountryCode, $userId)
    {
        return TokenHelper::create($this->createPasswordResetTokenObject($mobile, $mobileCountryCode, $userId));
    }

    private function createPasswordResetTokenObject($mobile, $mobileCountryCode, $userId)
    {
        return (object)[
            'auth' => 'resetPassword',
            'mobile' => $mobile,
            'mobileCountryCode' => $mobileCountryCode,
            'userId' => $userId
        ];
    }

    public function verifyPasswordResetToken($token, $mobile, $mobileCountryCode, $userId)
    {
        return TokenHelper::verify($token, $this->createPasswordResetTokenObject($mobile, $mobileCountryCode, $userId));
    }

}
