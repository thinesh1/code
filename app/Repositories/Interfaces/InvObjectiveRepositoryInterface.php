<?php

namespace App\Repositories\Interfaces;

Interface InvObjectiveRepositoryInterface
{
    public function createInvObjective($user_id,
    $investmentObjectives,
    $investorType
    );

}
