<?php

namespace App\Repositories\Interfaces;

use App\Models\User;

Interface UserRepositoryInterface
{
    public function createUnverifiedUser(
        $mobileCountryCode, $mobile,  $password, $email, $marketingConsentEmail = 0,
        $marketingConsentMobile = 0 );

    public function getUser($userId);

    public function updateUser($userId, $data);

    public function findUser($userId);

    public function getUserByMobile($mobileCountryCode, $mobile);

    public function updatePassword($password, $userId);
}
