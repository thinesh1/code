<?php

namespace App\Repositories\Interfaces;

Interface InvStatusRepositoryInterface
{
    public function createInvStatus($user_id,
    $investorStatusLastYearEarning,
    $investorStatusFinancialAssets,
    $investorStatusCheckbox,
    $investorStatusPersonalAssets
    );

}
