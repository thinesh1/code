<?php

namespace App\Repositories\Interfaces;

Interface EmpStatusRepositoryInterface
{
    public function createEmpStatus($user_id,
    $employmentStatus,
    $relatedToPubliclyTradedCompany,
    $workForAnotherStockBroker
    );

}
