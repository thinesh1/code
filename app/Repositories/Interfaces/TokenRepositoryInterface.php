<?php

namespace App\Repositories\Interfaces;

use App\Models\User;

Interface TokenRepositoryInterface
{
	public function clearRegistrationToken($mobile, $mobileCountryCode);

    public function createNewRegistrationToken($mobile,$mobileCountryCode);

    public function requestOTP($mobileCountryCode, $mobile);

    public function jwtAttempt($mobileCountryCode, $mobile, $password);

    public function jwtRefresh($refreshToken);

    public function verifyOTP($uuid, $otp, $mobile, $mobileCountryCode);

    public function getPasswordResetToken($mobile, $mobileCountryCode, $userId);

    public function verifyPasswordResetToken($token, $mobile, $mobileCountryCode, $userId);

}
