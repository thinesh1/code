<?php

namespace App\Repositories\Interfaces;

Interface InvExperienceRepositoryInterface
{
    public function createInvExperience( $user_id,
    $tradedSixTimesLastThreeYears,
    $higherQualificationInFinanceRelated,
    $anyFinanceRelatedQualification,
    $relevantWorkingExperienceOfThreeYears,
    $onlineEducationSGX
    );

}
