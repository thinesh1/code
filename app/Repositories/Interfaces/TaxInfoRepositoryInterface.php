<?php

namespace App\Repositories\Interfaces;

Interface TaxInfoRepositoryInterface
{
    public function createTaxInfo($user_id,
    $taxDifferentFromCountry,
    $multipleTaxResidencies,
    $usOrPermanentResident
    );

}
