<?php

namespace App\Repositories\Interfaces;

Interface EmpDetailsRepositoryInterface
{
    public function createEmpDetails($user_id,
    $industry,
    $employerName,
    $occupation,
    $annualIncome,
    $netWorth);

    public function createSourceOfWealth($sourceOfWealth);

}
