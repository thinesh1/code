<?php

namespace App\Repositories;

use App\Repositories\Interfaces\InvObjectiveRepositoryInterface;
use App\Models\InvObjective;

class InvObjectiveRepository implements InvObjectiveRepositoryInterface
{
    public function createInvObjective($user_id,
    $investmentObjectives,
    $investorType)
    {
        $dataSet = [
            'user_id' => $user_id,
            'investment_objectives' => $investmentObjectives,
            'investor_type' => $investorType
        ];
        return InvObjective::Create($dataSet);
    }
}
