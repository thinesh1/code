<?php

namespace App\Repositories;

use App\Repositories\Interfaces\EmpDetailsRepositoryInterface;
use App\Models\EmpDetails;
use App\Models\SourceOfWealth;

class EmpDetailsRepository implements EmpDetailsRepositoryInterface
{
    public function createEmpDetails($user_id,
    $industry,
    $employerName,
    $occupation,
    $annualIncome,
    $netWorth)
    {
        $dataSet = [
            'user_id' => $user_id,
            'industry' => $industry,
            'employer_name' => $employerName,
            'occupation' => $occupation,
            'annual_income' => $annualIncome,
            'net_worth'=>$netWorth
        ];

        return EmpDetails::Create($dataSet);
    }

    public function createSourceOfWealth($sourceOfWealth)
    {
        $dataSet = [
            'source_of_wealth' =>$sourceOfWealth,
            'emp_details_id'=>EmpDetails::pluck('id')->first()
        ];

        return SourceOfWealth::Create($dataSet);

    }

}
