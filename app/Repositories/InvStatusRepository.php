<?php

namespace App\Repositories;

use App\Repositories\Interfaces\InvStatusRepositoryInterface;
use App\Models\InvStatus;

class InvStatusRepository implements InvStatusRepositoryInterface
{
    public function createInvStatus($user_id,
    $investorStatusLastYearEarning,
    $investorStatusFinancialAssets,
    $investorStatusCheckbox,
    $investorStatusPersonalAssets)
    {
        $dataSet = [
            'user_id' => $user_id,
            'last_year_earning'=>$investorStatusLastYearEarning,
            'financial_assets'=>$investorStatusFinancialAssets,
            'personal_assets'=>$investorStatusPersonalAssets,
            'checkbox'=>$investorStatusCheckbox,
 
        ];
        return InvStatus::Create($dataSet);
    }
}
