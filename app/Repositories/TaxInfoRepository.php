<?php

namespace App\Repositories;

use App\Repositories\Interfaces\TaxInfoRepositoryInterface;
use App\Models\TaxInfo;


class TaxInfoRepository implements TaxInfoRepositoryInterface
{
    public function createTaxInfo($user_id,
    $usOrPermanentResident,
    $taxDifferentFromCountry,
    $multipleTaxResidencies)
    {
        $dataSet = [
            'user_id' => $user_id,
            'us_or_permanent_resident' => $usOrPermanentResident,
            'tax_different_from_country' => $taxDifferentFromCountry,
            'multiple_tax_residencies' => $multipleTaxResidencies,
        ];
        return TaxInfo::Create($dataSet);
    }
}
