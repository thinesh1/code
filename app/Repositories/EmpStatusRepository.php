<?php

namespace App\Repositories;

use App\Repositories\Interfaces\EmpStatusRepositoryInterface;
use App\Models\EmpStatus;

class EmpStatusRepository implements EmpStatusRepositoryInterface
{
    public function createEmpStatus($user_id,
    $employmentStatus,
    $relatedToPubliclyTradedCompany,
    $workForAnotherStockBroker
    )
    
    {
        $dataSet = [
            'user_id' => $user_id,
            'employment_status' => $employmentStatus,
            'related_to_publicly_traded_company' => $relatedToPubliclyTradedCompany,
            'work_for_another_stock_broker' => $workForAnotherStockBroker,
        ];
        return EmpStatus::Create($dataSet);
    }
}
