<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\EmpDetailsRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\EmpDetails;
use App\Requests\V1\EmpDetailsRequest;

class EmpDetailsController extends Controller
{
    protected $empDetailsRepository;

    public function __construct(EmpDetailsRepositoryInterface $empDetailsRepository)
    {
        $this->empDetailsRepository = $empDetailsRepository;
    }

    public function empDetailsSubmit(EmpDetailsRequest $request){
        
        $industry = $request->input('industry');
        $employerName = $request->input('employerName');
        $occupation = $request->input('occupation');
        $annualIncome = $request->input('annualIncome');
        $netWorth = $request->input('netWorth');
        $sourceOfWealth = $request->input('sourceOfWealth');
    
    
        $user_id = Auth::user()->id;
        
      //  $emp_details_id = EmpDetails::where('user_id', '=', $term)->first();

        $this->empDetailsRepository->createEmpDetails($user_id,
        $industry,
        $employerName,
        $occupation,
        $annualIncome,
        $netWorth
        );

        $this->empDetailsRepository->createSourceOfWealth($sourceOfWealth);
    }
}

