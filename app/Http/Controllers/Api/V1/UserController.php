<?php

namespace App\Http\Controllers\Api\V1;

use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Services\OnfidoService;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Auth;
use App\Requests\V1\AccountUpdateUserRequest;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getDetails()
    {
        return fractal(Auth::user(), new UserTransformer());
    }

    public function updateDetails(AccountUpdateUserRequest $request)
    {
        $userId = Auth::user()->id;
        $user = $this->userRepository->updateUser($userId,
            $request->only(array_keys($request->rules())));

        //$user = $this->userRepository->getUser($userId);
        return fractal($user, new UserTransformer());
    }

    public function getOnfidoSdkToken()
    {

        if (!isset(Auth::user()->first_name) ||
            empty(Auth::user()->first_name) ||
            !isset(Auth::user()->last_name) ||
            empty(Auth::user()->last_name)) {

            throw new AccessDeniedHttpException('INCOMPLETE USER DETAILS');

        }

        $onfidoService = new OnfidoService();
        $applicantId = $onfidoService->createApplicant(Auth::user()->first_name, Auth::user()->last_name);
        print_r($applicantId);
        $sdkToken = $onfidoService->getOnfidoSdkToken($applicantId);

        return [
            'data' => [
                'sdk_token' => $sdkToken
            ]
        ];
    }
}
