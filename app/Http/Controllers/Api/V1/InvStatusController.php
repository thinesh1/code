<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\InvStatusRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Requests\V1\InvStatusRequest;

class InvStatusController extends Controller
{
    protected $invstatusRepository;

    public function __construct(InvStatusRepositoryInterface $invstatusRepository)
    {
        $this->invstatusRepository = $invstatusRepository;
    }

    public function invStatusSubmit(InvStatusRequest $request){
    

        $investorStatusLastYearEarning = $request->input('investorStatusLastYearEarning');
        $investorStatusFinancialAssets = $request->input('investorStatusFinancialAssets');
        $investorStatusPersonalAssets = $request->input('investorStatusPersonalAssets');
        $investorStatusCheckbox = $request->input('investorStatusCheckbox');

        $user_id = Auth::user()->id;

        $this->invstatusRepository->createInvStatus(
        $user_id,
        $investorStatusLastYearEarning,
        $investorStatusFinancialAssets,
        $investorStatusPersonalAssets,
        $investorStatusCheckbox
        );
        
    }
}

