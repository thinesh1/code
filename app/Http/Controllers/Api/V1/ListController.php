<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Helpers\CountriesHelper;

class ListController extends Controller
{

    public function getNationalities()
    {
        return CountriesHelper::getCountries()->filter(function ($country) {

            return $country->nationality;

        })->values()->map(function ($country) {

            return [
                'type' => 'nationalities',
                'id' => $country->ISO2,
                'name' => $country->nationality
            ];
        });
    }

    public function getCountries()
    {
        return CountriesHelper::getCountries()->map(function ($country) {

            return [
                'type' => 'countries',
                'id' => $country->ISO2,
                'name' => $country->name
            ];
        });
    }

}
