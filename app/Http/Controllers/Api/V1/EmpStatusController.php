<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\EmpStatusRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Requests\V1\EmpStatusRequest;

class EmpStatusController extends Controller
{
    protected $empstatusRepository;

    public function __construct(EmpStatusRepositoryInterface $empstatusRepository)
    {
        $this->empstatusRepository = $empstatusRepository;
    }

    public function EmpStatusSubmit(EmpStatusRequest $request){

        $employmentStatus = $request->input('employmentStatus');
        $relatedToPubliclyTradedCompany = $request->input('relatedToPubliclyTradedCompany');
        $workForAnotherStockBroker = $request->input('workForAnotherStockBroker');
    
        $user_id = Auth::user()->id;

        $this->empstatusRepository->createEmpStatus($user_id,
        $employmentStatus,
        $relatedToPubliclyTradedCompany,
        $workForAnotherStockBroker
        );
        
    }
}

