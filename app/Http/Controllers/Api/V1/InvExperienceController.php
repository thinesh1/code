<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\InvExperienceRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Requests\V1\InvExperienceRequest;


class InvExperienceController extends Controller
{
    protected $invExperienceRepository;

    public function __construct(InvExperienceRepositoryInterface $invExperienceRepository)
    {
        $this->invExperienceRepository = $invExperienceRepository;
    }

    public function invExperienceSubmit(InvExperienceRequest $request){
      
        $tradedSixTimesLastThreeYears = $request->input('tradedSixTimesLastThreeYears');
        $higherQualificationInFinanceRelated = $request->input('higherQualificationInFinanceRelated');
        $anyFinanceRelatedQualification = $request->input('anyFinanceRelatedQualification');
        $relevantWorkingExperienceOfThreeYears = $request->input('relevantWorkingExperienceOfThreeYears');
        $onlineEducationSGX = $request->input('onlineEducationSGX');
    

        $user_id = Auth::user()->id;

        $this->invExperienceRepository->createInvExperience(
        $user_id,
        $tradedSixTimesLastThreeYears,
        $higherQualificationInFinanceRelated,
        $anyFinanceRelatedQualification,
        $relevantWorkingExperienceOfThreeYears,
        $onlineEducationSGX
        );
        
    }
}

