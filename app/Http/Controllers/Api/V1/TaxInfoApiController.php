<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\TaxInfoRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\TaxInfo;
use App\Requests\V1\TaxInfoRequest;

class TaxInfoApiController extends Controller
{
    protected $taxinfoRepository;

    public function __construct(TaxInfoRepositoryInterface $taxinfoRepository)
    {
        $this->taxinfoRepository = $taxinfoRepository;
    }

    public function taxSubmit(TaxInfoRequest $request){
        
        $usOrPermanentResident = $request->input('usOrPermanentResident');
        $taxDifferentFromCountry = $request->input('taxDifferentFromCountry');
        $multipleTaxResidencies = $request->input('multipleTaxResidencies');
       
        $user_id = Auth::user()->id;

        $this->taxinfoRepository->createTaxInfo($user_id,
        $usOrPermanentResident,
        $taxDifferentFromCountry,
        $multipleTaxResidencies
        );
    
    }
}

