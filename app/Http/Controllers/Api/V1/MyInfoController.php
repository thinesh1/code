<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Services\MyInfo\MyInfoAuthService;

class MyInfoController extends Controller
{
    protected $myInfoService;

    public function callback(Request $request)
    {
        $code = $request->input('code', '');
        Log::debug("Code before redirecting $code");

        return 'Redirecting';
    }

    public function getConsent()
    {
        return redirect($this->myInfoService->getAuthenticationUrl());
    }

    public function __construct()
    {
        $this->myInfoService = new MyInfoAuthService();
    }
}
