<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\UserMyinfo;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\TokenRepositoryInterface;
use App\Requests\V1\AuthRegisterUserRequest;
use App\Requests\V1\AuthRequestOTPRequest;
use App\Requests\V1\AuthResetPasswordRequest;
use App\Requests\V1\AuthVerifyPasswordRequest;
use App\Requests\V1\AuthPasswordChangeRequest;
use App\Helpers\TokenHelper;
use App\Transformers\UserTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Requests\V1\AuthTokenRequest;
use App\Requests\V1\AuthVerifyOTPRequest;
use App\Services\MyInfo\MyInfoAuthService;
use App\Requests\V1\MyInfoUserRequest;
use App\Transformers\MyInfoUserTransformer;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Exception;

class AuthController extends Controller
{
    protected $userRepository;
    protected $tokenRepository;
    protected $myInfoAuthService;

    public function __construct(UserRepositoryInterface $userRepository, TokenRepositoryInterface $tokenRepository)
    {
        $this->userRepository = $userRepository;
        $this->tokenRepository = $tokenRepository;
        $this->myInfoAuthService = new MyInfoAuthService();
    }

    public function registerUser(AuthRegisterUserRequest $request)
    {
        $data = $request->all();

        $marketingConsentEmail = 0;
        $marketingConsentMobile = 0;
        if (isset($data['marketingConsent']) && $data['marketingConsent'] == 1) {
            $marketingConsentEmail = 1;
            $marketingConsentMobile = 1;
        }

        $mobileCountryCode = $request->input('mobileCountryCode');
        $mobile = $request->input('mobile');

        $user = $this->userRepository->createUnverifiedUser(
            $mobileCountryCode,
            $mobile,
            $request->input('password'),
            $request->input('email'),
            $marketingConsentEmail,
            $marketingConsentMobile
        );

        $this->tokenRepository->clearRegistrationToken(
            $mobileCountryCode,
            $mobile
        );

        $registrationToken = $this->tokenRepository->createNewRegistrationToken($mobile, $mobileCountryCode);
        $response = fractal($user, new UserTransformer())->toArray();
        $response['data']['registrationToken'] = $registrationToken;
        return $response;
    }

    public function token(AuthTokenRequest $request)
    {
        $token = $this->tokenRepository->jwtAttempt(
            $request->input('mobileCountryCode'),
            $request->input('mobile'),
            $request->input('password')
        );

        if($token){
            return [
                'data' => $token
            ];
        }

        throw new UnauthorizedHttpException('', 'Could not authenticate user');
    }

    public function refresh(Request $request)
    {
        $token = $this->tokenRepository->jwtRefresh($request->input('refreshToken'));
        if($token){
            return [
                'data' => $token
            ];
        }

        throw new BadRequestHttpException(__('api.invalid_refresh_code'));
    }

    public function userInfo()
    {

    }

    public function requestOTP(AuthRequestOTPRequest $request)
    {
        $userId = $request->input('userId');
        $user = $this->userRepository->getUser($userId);

        if (!$user)
        {
            throw new BadRequestHttpException(__('auth.user_unavailable'));
        }

        try {
            $uuid = $this->tokenRepository->requestOTP(
                $user->mobile_country_code,
                $user->mobile
            );

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return [
                'message' => 'Invalid mobile number'
            ];
        }

        if (!$uuid) {
            throw new UnprocessableEntityHttpException();
        }

        return [
            'message' => trans('auth.otp_sent'),
            'data' => [
                'uuid' => $uuid
            ]
        ];

    }

    public function verifyOTP(AuthVerifyOTPRequest $request)
    {
        $userId = $request->input('userId');
        $user = $this->userRepository->getUser($userId);
        if (!$user)
        {
            throw new BadRequestHttpException(__('auth.user_unavailable'));
        }

        $result = $this->tokenRepository->verifyOTP(
            $request->input('uuid'),
            $request->input('otp'),
            $user->mobile,
            $user->mobile_country_code

        );

        if(!$result){
            throw new UnauthorizedHttpException('', 'Incorrect OTP or OTP expired');
        }

        $user->mobile_verified_at = Carbon::now();
        $user->save();

        return [
            'message' => trans('auth.otp_verified'),
            'data' => [

            ]
        ];
    }

    public function passwordResetRequest(AuthResetPasswordRequest $request)
    {
        $mobileCountryCode = $request->input('mobileCountryCode');
        $mobile = $request->input('mobile');

        $user = $this->userRepository->getUserByMobile(
            $mobileCountryCode,
            $mobile
        );

        if(!$user){
            throw new UnprocessableEntityHttpException(__('validation.password_reset_mobile', ['mobile' => $request->input('mobileCountryCode') . $request->input('mobile')]));
        }

        $uuid = $this->tokenRepository->requestOTP(
            $mobileCountryCode,
            $mobile
        );

        if(!$uuid){

            throw new UnprocessableEntityHttpException('We are unable to identify your mobile number +'.$request->input('mobileCountryCode').' '.$request->input('mobile').'. If you have an account or debit card with us, please use the mobile number provided to us at sign up or you may sign up now.');
        }

        return [
            'message' => trans('auth.otp_sent'),
            'data' => [
                'uuid' => $uuid,
                'mobileCountryCode' => $mobileCountryCode,
                'mobile' => $mobile,
            ]
        ];
    }

    public function passwordVerify(AuthVerifyPasswordRequest  $request)
    {

        $result = $this->tokenRepository->verifyOTP(
            $request->input('uuid'),
            $request->input('otp'),
            $request->input('mobile'),
            $request->input('mobileCountryCode')
        );

        if(!$result){

            throw new UnauthorizedHttpException('', 'Incorrect OTP or OTP expired');
        }

        $user = $this->userRepository->getUserByMobile(
            $request->input('mobileCountryCode'),
            $request->input('mobile')
        );

        return [
            'data' => [
                'passwordResetToken' => $this->tokenRepository->getPasswordResetToken(
                    $request->input('mobile'),
                    $request->input('mobileCountryCode'),
                    $user->id
                ),
            ]
        ];
    }

    public function passwordChange(AuthPasswordChangeRequest $request)
    {

        $decryptedToken = TokenHelper::decrypt($request->input('passwordResetToken'));

        $user = $this->userRepository->getUserByMobile(
            $decryptedToken->data->mobileCountryCode,
            $decryptedToken->data->mobile
        );

        if(!$user){

            throw new UnprocessableEntityHttpException();
        }

        $result = $this->tokenRepository->verifyPasswordResetToken(
            $request->input('passwordResetToken'),
            $user->mobile,
            $user->mobile_country_code,
            $user->id
        );

        if(!$result){

            throw new UnauthorizedHttpException('', 'Incorrect token or token expired');
        }

        if ($request->input('idNumber') != '')
        {
            $user = $this->userRepository->getUserByIdNumber(
                $request->input('idNumber')
            );
        }

        $this->userRepository->updatePassword(
            $request->input('password'),
            $user->id
        );

        return [
            'message' => trans('passwords.reset')
        ];
    }

    public function getMyInfoAuthUrl()
    {
        $url = $this->myInfoAuthService->getAuthenticationUrl();

        Log::info("Myinfo Auth Url:" . $url);
        return [
            'data' => [
                'redirectUrl' => $url
            ]
        ];
    }

    public function getMyInfoUser(MyInfoUserRequest $request)
    {
        $code = $request->input('code');
        Log::debug("getMyInfoUser:" . $code);

        $myinfoUser = $this->myInfoAuthService->getMyinfoPersonData($code);

        Log::debug(json_encode($myinfoUser));

        /*
         * Save myInfo data
         */

        try{

            UserMyinfo::updateOrCreate(
                [
                    'user_id' => Auth::id(),
                ],
                [
                    'myinfo_ref' => $code,
                    'myinfo_response' => json_encode($myinfoUser),

                ]);

        }catch (Exception $exception){

            Log::error('Error while saving myinfo data');
        }

        return fractal($myinfoUser, new MyInfoUserTransformer());
    }
}
