<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\InvObjectiveRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\InvObjective;
use App\Requests\V1\InvObjectiveRequest;

class InvObjectiveController extends Controller
{
    protected $invObjectiveRepository;

    public function __construct(InvObjectiveRepositoryInterface $invObjectiveRepository)
    {
        $this->invObjectiveRepository = $invObjectiveRepository;
    }

    public function invObjectiveSubmit(InvObjectiveRequest $request){

        $investmentObjectives = $request->input('investmentObjectives');
        $investorType = $request->input('investorType');
    
        $user_id = Auth::user()->id;

        $this->invObjectiveRepository->createInvObjective($user_id,
        $investmentObjectives,
        $investorType
        );
        
    }
}

