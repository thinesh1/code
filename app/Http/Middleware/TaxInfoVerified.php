<?php

namespace App\Http\Middleware;

use App\Models\TaxInfo;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


class TaxInfoVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
       
        $user = TaxInfo::where('user_id', '=', Auth::user()->id)->first();
        if ($user === null) {
           throw new AccessDeniedHttpException('TAX_INFO_NOT_VERIFIED');
        }
        return $next($request);
    }
}
