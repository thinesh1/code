<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\InvStatus;
use App\Models\EmpStatus;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class EmpStatusVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $term=InvStatus::get(['user_id'])->pluck('user_id');
        $user = EmpStatus::where('user_id', '=', $term)->first();
        if ($user === null) {
           throw new AccessDeniedHttpException('EMPLOYMENT_STATUS_NOT_VERIFIED');
        }
        return $next($request);
    }
}
