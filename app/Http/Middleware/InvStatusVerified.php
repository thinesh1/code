<?php

namespace App\Http\Middleware;

use App\Models\InvStatus;
use App\Models\TaxInfo;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


class InvStatusVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $term=TaxInfo::get(['user_id'])->pluck('user_id');
        $user = InvStatus::where('user_id', '=', $term)->first();
        if ($user === null) {
           throw new AccessDeniedHttpException('INVESTOR_STATUS_NOT_VERIFIED');
        }
        return $next($request);
    }
}
