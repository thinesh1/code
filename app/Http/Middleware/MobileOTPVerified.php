<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class MobileOTPVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!isset(Auth::user()->mobile_verified_at) || empty(Auth::user()->mobile_verified_at)) {
            throw new AccessDeniedHttpException('MOBILE_NOT_VERIFIED');
        }

        return $next($request);
    }
}
