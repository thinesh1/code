<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        $details = [
            'id' => $user->id,
            'type' => 'users',
            'attributes' => [
                'firstName' => $user->first_name ?? null,
                'lastName' => $user->last_name ?? null,
                'gender' => $user->gender ?? null,
                'email' => $user->email,
                'mobile' => [
                    'countryCode' => $user->mobile_country_code,
                    'number' => $user->mobile
                ],
                'dateOfBirth' => $user->date_of_birth ?? null,
                'nationality' => $user->nationality ?? null,
                'address' => [
                    'number' => $user->address_number ?? null,
                    'refinement' => $user->address_refinement ?? null,
                    'blockNumber' => $user->address_block_number ?? null,
                    'region' => $user->address_region ?? null,
                    'street' => $user->address_street ?? null,
                    'city' => $user->address_city ?? null,
                    'postalCode' => $user->address_postal_code ?? null,
                    'countryCode' => $user->address_country_code ?? null,
                ],
                'avatar' => [
                    'url' => $user->avatar ?? null,
                ],
                'kycStatus' => $user->kyc_status ?? 'NOT_VERIFIED',
                'idType' => $user->id_type,
                'idNumber' => $user->id_number,
            ]
        ];

        return $details;

    }
}

