<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class MyInfoUserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param $response
     * @return array
     */
    public function transform($response)
    {
        return [
            'uinfin' => $response->uinfin->value ?? '',
            'name' => $response->name->value ?? '',
            'gender' => $response->sex->code ?? '',
            'nationality' => $response->nationality->code ?? '',
            'dateOfBirth' => $response->dob->value ?? '',
//            'countryOfBirth' => $response->birthcountry->code ?? '',
            'countryOfBirth' => $response->nationality->code ?? '',
            'passExpireDate' => $response->passexpirydate->value ?? '',
            'address' => [
                'buildingNumber' => $response->regadd->block ?? '',
                'refinement' => $response->regadd->unit ?? '',
                'floor' => $response->regadd->floor ?? '',
                'region' => null,
                'street' => $response->regadd->street ?? '',
                'city' => null,
                'postalCode' => $response->regadd->postal ?? '',
                'countryCode' => $response->regadd->country ?? '',
                'building' => $response->regadd->building ?? '',
                'type' => $response->regadd->type ?? '',
                'line1' => $response->regadd->line1 ?? '',
                'line2' => $response->regadd->line2 ?? '',
            ],
        ];
    }
}
