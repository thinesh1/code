<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'nexmo' => [

        'rest_base_url' => env('NEXMO_REST_BASE_URL'),
        'api_base_url' => env('NEXMO_API_BASE_URL'),
        'api_key' => env('NEXMO_API_KEY'),
        'key' => env('NEXMO_KEY'),
        'api_secret' => env('NEXMO_API_SECRET'),
        'secret' => env('NEXMO_SECRET'),
        'brand' => env('NEXMO_APP_BRAND','Canvas'),
        'sender_id' => env('NEXMO_SENDER_ID','Canvas'),
        'sms_from' => env('NEXMO_SENDER_ID','Canvas'),
        'code_length' => env('NEXMO_CODE_LENGTH',6),
        'pin_expiry' => env('NEXMO_PIN_EXPIRES',240),
        'next_event_wait' => env('NEXMO_NEXT_EVENT_IN',60),//https://developer.nexmo.com/verify/guides/changing-default-timings
        'workflow_id' => env('NEXMO_WORKFLOW_ID',4),//https://developer.nexmo.com/verify/guides/workflows-and-events
    ],

    'myinfo' => [
        'base_url' => env('MYINFO_BASE_URL'),
        'callback' => env('MYINFO_CALLBACK'),
        'client_id' => env('MYINFO_CLIENT_ID'),
        'secret' => env('MYINFO_CLIENT_SECRET'),
        'client_auth_level' => env('MYINFO_CLIENT_AUTH_LEVEL','L2'),
        //'publicCert' => env('MYINFO_PUBLIC_CERT', $publicCert),
        'publicCertPath' => env('MYINFO_PUBLIC_CERT_PATH'),
        'private_key_path' => env('MYINFO_PRIVATE_KEY_PATH'),
        'purpose' => env('MYINFO_PURPOSE','This Information is required as per regulatory compliance set by the Monetary Authority of Singapore (MAS).'),
    ],

    'onfido' => [
        'api_key' => env('ONFIDO_API_KEY'),
        'application_id' => env('ONFIDO_APP_ID'),
    ],

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

];
