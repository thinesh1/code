<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api->version('v1', function ($api) {
    $api->put('/auth/register', ['as' => 'auth.register', 'uses' => 'AuthController@registerUser']);
    $api->get('/auth/user-info', ['as' => 'auth.user.info', 'uses' => 'AuthController@userInfo']);
    $api->post('/auth/token', ['as' => 'auth.token', 'uses' => 'AuthController@token']);
    $api->post('/auth/refresh', ['as' => 'auth.refresh', 'uses' => 'AuthController@refresh']);

    $api->post('/auth/password-reset', ['as' => 'auth.password.reset', 'uses' => 'AuthController@passwordResetRequest']);
    $api->post('/auth/password-verify', ['as' => 'auth.password.verify', 'uses' => 'AuthController@passwordVerify']);
    $api->post('/auth/password-change', ['as' => 'auth.password.change', 'uses' => 'AuthController@passwordChange']);

    $api->get('/list/countries', ['as' => 'list.countries', 'uses' => 'ListController@getCountries']);
    $api->get('/list/nationalities', ['as' => 'list.nationalities', 'uses' => 'ListController@getNationalities']);

    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->post('/auth/otp-request', ['as' => 'auth.otp.request', 'uses' => 'AuthController@requestOTP']);
        $api->post('/auth/otp-verify', ['as' => 'auth.otp.verify', 'uses' => 'AuthController@verifyOTP']);

        $api->post('/auth/tax-information',['as' => 'auth.otp.tax-information', 'uses' =>'TaxInfoApiController@taxSubmit']);

        $api->get('/auth/myinfo-url', ['as' => 'auth.myinfo-url', 'uses' => 'AuthController@getMyInfoAuthUrl']);
        $api->post('/auth/myinfo-user', ['as' => 'auth.myinfo-user', 'uses' => 'AuthController@getMyInfoUser']);

        $api->get('/user/verify/onfido-sdk-token', ['as' => 'user.verify.onfido', 'uses' => 'UserController@getOnfidoSdkToken']);

        $api->get('/user/details', ['as' => 'user.details.get', 'uses' => 'UserController@getDetails']);

        $api->group(['middleware' => 'mobile.verified'], function ($api) {
            $api->put('/user/details', ['as' => 'user.details.update', 'uses' => 'UserController@updateDetails']);
        });

        $api->group(['middleware' => 'tax.verified'], function ($api) {
            $api->post('/auth/investor-status',['as' => 'auth.otp.investor-status', 'uses' =>'InvStatusController@invStatusSubmit']);
            
           
            $api->group(['middleware' => 'invstatus.verified'], function ($api) { 
                $api->post('/auth/employment-status',['as' => 'auth.otp.employment-status', 'uses' =>'EmpStatusController@empStatusSubmit']);
                $api->post('/auth/investment-objectives',['as' => 'auth.otp.investment-objectives', 'uses' =>'InvObjectiveController@invObjectiveSubmit']);
               
                $api->group(['middleware' => 'empstatus.verified'], function ($api) { 
                    $api->post('/auth/investment-experience',['as' => 'auth.otp.investment-experience', 'uses' =>'InvExperienceController@invExperienceSubmit']); 
                });    
            });

        });

    });

});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
